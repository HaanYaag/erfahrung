1. 在ubuntu中安装gstreamer

```
apt-get install libgstreamer1.0-0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio
```


2. 编译

```
gcc basic-tutorial-1.c -o basic-tutorial-1 `pkg-config --cflags --libs gstreamer-1.0`
```



gst-launch-1.0

https://gstreamer.freedesktop.org/documentation/tools/gst-launch.html?gi-language=c



gst_element_factory_make()

The first parameter is the type of element to create
The second parameter is the name we want to give to this particular instance.


gst_bin_add_many (GST_BIN (pipeline), source, sink, NULL);

添加 source, sink, 结尾是NULL在pipeline里

gst_element_link()

Its first parameter is the source, and the second one the destination
把两个element连接在一起，但是必须是同一个bin里的

/* Modify the source's properties */
g_object_set (source, "pattern", 0, NULL);

The line of code above changes the “pattern” property of videotestsrc, which controls the type of test video the element outputs. Try different values!

names and possible values of all the properties an element exposes can be found using the gst-inspect-1.0 tool


g_object_set (data.source, "uri", "https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm", NULL);

We set the URI of the file to play via a property

/* Connect to the pad-added signal */
g_signal_connect (data.source, "pad-added", G_CALLBACK (pad_added_handler), &data);

In this line, we are attaching to the “pad-added” signal of our source (an uridecodebin element). To do so, we use g_signal_connect() and provide the callback function to be used (pad_added_handler) and a data pointer. GStreamer does nothing with this data pointer, it just forwards it to the callback so we can share information with it. In this case, we pass a pointer to the CustomData structure we built specially for this purpose.

g_signal_connect() 把 “pad-added” signal 附带在 source 上， ，提供一个callback，同时附带一个data指针，data是CustomData数据结构的实例，data的作用是分享信息

source是触发的对象

GstElement 会有哪些信号呢？



static void pad_added_handler (GstElement *src, GstPad *new_pad, CustomData *data) {

callback, signal handler

new_pad is the GstPad that has just been added to the src element. 

data is the pointer we provided when attaching to the signal.

GstPad *sink_pad = gst_element_get_static_pad (data->convert, "sink");

提取 convert element中的名为sink pad。
(从哪提取，提取谁)
From CustomData we extract the converter element, and then retrieve its sink pad using gst_element_get_static_pad (). This is the pad to which we want to link new_pad.

## type and capabilities

capabilities of the pad = that is, the kind of data it currently outputs

The current caps on a pad will always have a single GstStructure and represent a single media format, or if there are no current caps yet NULL will be returned.

/* Check the new pad's type */
new_pad_caps = gst_pad_get_current_caps (new_pad, NULL);
new_pad_struct = gst_caps_get_structure (new_pad_caps, 0);
new_pad_type = gst_structure_get_name (new_pad_struct);
if (!g_str_has_prefix (new_pad_type, "audio/x-raw")) {
  g_print ("It has type '%s' which is not raw audio. Ignoring.\n", new_pad_type);
  goto exit;
}

gst_pad_get_current_caps (new_pad, NULL);
返回new_pad这个pad目前通过的数据类型，如果没有数据则返回NULL，返回值是GstCaps类型。
GstCaps类型包含很多种GstStructure类型

gst_caps_get_structure (new_pad_caps, 0);
new_pad_caps是一个GstCaps，其中包含很多GstStructure，返回这个GstCaps的第0个GstStructure类型

gst_structure_get_name(GstStructure)

得到这个GstStructure的名字

g_str_has_prefix (new_pad_type, "audio/x-raw")
比较两个字符串是否一致

gst_pad_link() 连接两个pad

gst_element_link()连接两个element

都是从source到sink





