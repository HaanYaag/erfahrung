* str = qwe123
* *str = q
* str+1 = we123

char* argument = NULL;
char size[strlen(argument)]; 
size[0] = 0;

strcpy(char*,"a")这样可以吗？
好像不可以吧？
strcpy(cahr[],"a")
这样才行吧？

```
char aa[3]="123";
printf("aa=%s\n",aa);
int i=0;
while(aa[i]!='\0'){
    printf("while aa[%d]=%c\n",i,aa[i]);
    i++;
}
```

>aa=123
while aa[0]=1
while aa[1]=2
while aa[2]=3

--------

```
char bb[4]="123";
printf("bb=%s\n",bb);
 i=0;
while(bb[i]!='\0'){
    printf("while aa[%d]=%c\n",i,bb[i]);
    i++;
}
```
>bb=123
while aa[0]=1
while aa[1]=2
while aa[2]=3

--------

```
char* a = "123";
printf("a=%s\n",a);
```
>a=123

--------

```
char b[10]="";
strncat(b,a,5);
printf("b=%s\n",b);
i=0;
while(b[i]!='\0'){
    printf("while b[%d]=%c\n",i,b[i]);
    i++;
}
```

>b=123
while b[0]=1
while b[1]=2
while b[2]=3

--------

```
char*c = b;
printf("c= %s\n",c);
```
>c= 123


to inizialise a char[]
```
	char d[strlen(str) + 10];
	d[0] = 0;
```

------------------

把str中的数字导出到re
**但是还是会在调用这个函数的函数的返回值中出现乱码现象**
解决办法是用`strcpy`将结果存在另一个`char*`中

```
@input: char* str;
@output: char re[];

char re[strlen(str) + 10];
re[0] = 0;
while(*str!=0){
	printf("str: %c\n",*str);
	if(*str>'0'&&*str<'9'){
		printf(" ->this is a number\n");
		strncat(re, str, 1);
	}
	++str;
}
```
也可以用`re[1]=*str`的方式来赋值

------------------

问题：
为什么下面这一段的返回值会出现乱码？
```
for (int i = 0; i < strlen(str); i++) {
	printf("%d: %s\n", i, str + i);
	printf("strncmp(a + i, 0, 1) %d \n", strncmp(str + i, "0", 1));
	if (strncmp(str + i, "0", 1) >= 0 && strncmp(str + i, "9", 1) <= 0) {
		printf(" >this is a number\n");
		strncat(re, str + i, 1);
	}
}
```
这一段的比较是用strncmp，读取下一个值是用str+i处理指针的位置，用strn系列函数的最后一个参数来确定数据的长度。
问题在于，这个方法得到的re[]，再函数的输出的时候没有问题
但是在主函数中调用这个函数就会出现乱码的结果
>函数中的结果：result = q1234567
函数中的地址：result = 0x7ffe93398770
调用的结果：result=�w�
调用的地址：result = 0x7ffe93398770
原因目前不明




----------------------

int stat(const char *pathname, struct stat *statbuf);
struct stat sb;
if (stat(argv[1], &sb) == -1) 